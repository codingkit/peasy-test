# Peasy Test Application

### Description

This application allows users to add, edit and delete details such as name, phone number and email.

### How to setup

- Run `npm install` or `yarn`
- Run `npm start` or `yarn start` and test the application on `http://localhost:3000/`

### Dependencies

- React
- Material UI
- Webpack
