import React, { useState, useEffect } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";

import { makeStyles } from "@material-ui/core/styles";

import TopBarProgress from "react-topbar-progress-indicator";
import { v4 as uuidv4 } from "uuid";
import { withSnackbar } from "notistack";
import PropTypes from "prop-types";

import DetailsTable from "./components/Table.jsx";
import DetailsModal from "./components/Modal.jsx";

import axiosInstance from "./utils/axiosInstance";

const useStyles = makeStyles(theme => ({
  box: {
    position: "relative",
  },
  margin: {
    margin: theme.spacing(2),
    // float: "right",
    position: "absolute",
    right: 0,
  },
}));

TopBarProgress.config({
  barColors: {
    "0": "#f00",
    "0.5": "#0f0",
    "1.0": "#00f",
  },
  shadowBlur: 5,
});

const App = ({ enqueueSnackbar }) => {
  const classes = useStyles();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [detail, setDetail] = useState({
    id: uuidv4(),
    name: "",
    email: "",
    phone: "",
  });
  const [details, setDetails] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [newList, setNewList] = useState(false);
  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    setLoading(true);
    axiosInstance.get("details/").then(({ data }) => {
      setLoading(false);
      setDetails(data);
    });
  }, [newList]);

  const handleSave = e => {
    e.preventDefault();
    const { name, email, phone } = detail;
    if (!name || !email || !phone) {
      setError("Please enter all details");
      return;
    }
    setLoading(true);
    if (!isEdit) {
      axiosInstance
        .post("details/", { ...detail })
        .then(() => {
          setNewList(true);
          setLoading(false);
          enqueueSnackbar("Save successful", {
            variant: "success",
          });
        })
        .catch(() => {
          enqueueSnackbar("Save failed", {
            variant: "error",
          });
          setLoading(false);
        });
    } else {
      axiosInstance
        .put(`details/${detail.id}`, { ...detail })
        .then(() => {
          setNewList(true);
          setLoading(false);
          enqueueSnackbar("Edit successful", {
            variant: "success",
          });
        })
        .catch(() => {
          enqueueSnackbar("Edit failed", {
            variant: "error",
          });
          setLoading(false);
        });
    }
    setNewList(false);
    setIsModalOpen(!isModalOpen);
  };

  const handleEdit = row => {
    setDetail({ ...row });
    setIsModalOpen(true);
    setIsEdit(true);
  };

  const handleDelete = row => {
    setLoading(true);
    axiosInstance
      .delete(`details/${row.id}`)
      .then(() => {
        setNewList(true);
        setLoading(false);
        enqueueSnackbar("Delete successful", {
          variant: "success",
        });
      })
      .catch(() => {
        enqueueSnackbar("Delete failed", {
          variant: "error",
        });
        setLoading(false);
      });
    setNewList(false);
  };

  const handleOnChange = e => {
    setDetail({
      ...detail,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnClick = e => {
    setIsModalOpen(!isModalOpen);
    setError("");
    setIsEdit(false);
    setDetail({
      id: uuidv4(),
      name: "",
      email: "",
      phone: "",
    });
  };

  return (
    <Container maxWidth="md">
      {loading && <TopBarProgress />}
      <Box my={4} className={classes.box}>
        <Typography variant="h4" component="h1" gutterBottom align="center">
          Detail's Table
        </Typography>
        <DetailsTable
          details={details}
          handleEdit={handleEdit}
          handleDelete={handleDelete}
        />
        <DetailsModal
          isModalOpen={isModalOpen}
          onClick={handleOnClick}
          detail={detail}
          handleOnChange={handleOnChange}
          handleSave={handleSave}
          error={error}
          isEdit={isEdit}
        />
        <Fab
          color="primary"
          aria-label="add"
          className={classes.margin}
          onClick={handleOnClick}
        >
          <AddIcon />
        </Fab>
      </Box>
    </Container>
  );
};

App.propTypes = {
  enqueueSnackbar: PropTypes.func.isRequired,
};

export default withSnackbar(App);
