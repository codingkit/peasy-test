import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import PropTypes from "prop-types";

import Form from "./Form.jsx";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  paper: {
    position: "absolute",
    width: "auto",
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.spacing(2),
    padding: theme.spacing(2, 4, 3),
    outline: "none",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  title: {
    margin: 0,
    padding: theme.spacing(1),
  },
  error: {
    color: "#F44337",
  },
}));

const Title = ({ children, onClose }) => {
  const classes = useStyles();
  return (
    <MuiDialogTitle disableTypography className={classes.title}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
};

Title.propTypes = {
  onClose: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
};

const DetailsModal = ({
  isModalOpen,
  onClick,
  handleOnChange,
  handleSave,
  error,
  detail,
  isEdit,
}) => {
  const classes = useStyles();
  return (
    <Modal
      aria-labelledby="details-modal"
      aria-describedby="add-or-edit-details"
      open={isModalOpen}
      onClose={onClick}
      className={classes.modal}
    >
      <div className={classes.paper}>
        <Title onClose={onClick}>
          {detail && detail.name ? <p>Edit Details</p> : <p>Add Details</p>}
        </Title>
        {error && (
          <Typography className={classes.error} variant="inherit">
            {error}
          </Typography>
        )}
        <Form
          handleOnChange={handleOnChange}
          handleCancel={onClick}
          handleSave={handleSave}
          detail={detail}
          isEdit={isEdit}
        />
      </div>
    </Modal>
  );
};

DetailsModal.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  handleOnChange: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  error: PropTypes.string,
  detail: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
  }).isRequired,
  isEdit: PropTypes.bool.isRequired,
};

DetailsModal.defaultProps = {
  error: "",
};

export default DetailsModal;
