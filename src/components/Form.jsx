import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";

import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  form: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  input: {
    margin: theme.spacing(2),
  },
  buttons: {
    display: "flex",
    float: "right",
  },
  button: {
    marginRight: theme.spacing(2),
  },
  appbar: {},
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const Form = ({ handleOnChange, handleCancel, handleSave, detail, isEdit }) => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const classes = useStyles();
  const { name, email, phone } = detail;
  return (
    <form noValidate autoComplete="off" className={classes.form}>
      <Paper elevation={0} className={classes.appbar}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab label="Name" />
          <Tab label="Email" />
          <Tab label="Phone" />
        </Tabs>
      </Paper>
      <TabPanel value={value} index={0}>
        <TextField
          variant="outlined"
          type="text"
          name="name"
          value={name}
          className={classes.input}
          onChange={handleOnChange}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <TextField
          variant="outlined"
          type="email"
          name="email"
          value={email}
          className={classes.input}
          onChange={handleOnChange}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <TextField
          variant="outlined"
          type="text"
          name="phone"
          value={phone}
          className={classes.input}
          onChange={handleOnChange}
        />
      </TabPanel>

      <div className={classes.buttons}>
        <Button
          variant="contained"
          className={classes.button}
          onClick={handleCancel}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleSave}
        >
          {isEdit ? "Save" : "Add"}
        </Button>
      </div>
    </form>
  );
};

Form.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  detail: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
  }).isRequired,
};

export default Form;
