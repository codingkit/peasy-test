import React from "react";
import ReactDOM from "react-dom";
import { SnackbarProvider } from "notistack";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import { purple, teal } from "@material-ui/core/colors";

import App from "./App.jsx";

//  change primary color
const theme = createMuiTheme({
  palette: {
    primary: { main: purple[500] },
    secondary: { main: teal[200] },
  },
});

ReactDOM.render(
  <SnackbarProvider
    maxSnack={3}
    anchorOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
  >
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </SnackbarProvider>,
  document.getElementById("main"),
);
